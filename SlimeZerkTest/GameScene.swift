//
//  GameScene.swift
//  SlimeZerkTest
//
//  Created by Parrot on 2019-02-25.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    // MARK: Variables for tracking time
    private var lastUpdateTime : TimeInterval = 0
    
    // MARK: Sprite variables
    var player:SKSpriteNode = SKSpriteNode()
    var upButton:SKSpriteNode = SKSpriteNode()
    var downButton:SKSpriteNode = SKSpriteNode()
    
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    
    
    // MARK: Scoring and Lives variables
    
    
    // MARK: Game state variables
    
    
    
    // MARK: Default GameScene functions
    // -------------------------------------
    override func didMove(to view: SKView) {
        self.lastUpdateTime = 0
        
        // get sprites from Scene Editor
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.upButton = self.childNode(withName: "upButton") as! SKSpriteNode
        self.downButton = self.childNode(withName: "downButton") as! SKSpriteNode
        
        
        // get labels from Scene Editor
        self.livesLabel = self.childNode(withName: "livesLabel") as! SKLabelNode
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
       
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }

        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
    
        // HINT: This code prints "Hello world" every 5 seconds
        if (dt > 5) {
            print("HELLO WORLD!")
            self.lastUpdateTime = currentTime
        }
        
    }
    
    // MARK: Custom GameScene Functions
    // Your custom functions can go here
    // -------------------------------------
    
    
}
